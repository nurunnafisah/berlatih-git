<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/', [AuthController::class, 'index']);
Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [AuthController::class, 'index']);
Route::post('signUp', [AuthController::class, 'indexWelcome']);

Route::get('/master', function () {
  return view('layouts.master');
});
Route::get('/table', function () {
  return view('content.table');
});
Route::get('/data-table', function () {
  return view('content.data-table');
});

// CRUD Cast
// Create Data 
// Mengarah ke form tambah data
Route::get('/cast/create', [CastController::class, 'create']);
// Menyimpan data ke table cast db
Route::post('/cast', [CastController::class, 'store']);

// Read Data
// Mengambil semua data di database
Route::get('/cast', [CastController::class, 'index']);
// Detail kategori ambil berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// Update data berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);